package lt.mesgalis.convolution.bit.test;

import lt.mesgalis.convolution.bit.Bit;
import lt.mesgalis.convolution.bit.BitUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Karolis on 2014-11-04.
 */
public class OperationsTest {

    @Test
    public void testXOR() {
        Set<Bit> bits = new HashSet<Bit>(BitUtils.bitstring2BitList("010"));
        Bit xorResult = BitUtils.xor(bits);
        Assert.assertTrue(xorResult.getValue() == true);

        bits = new HashSet<Bit>(BitUtils.bitstring2BitList("101"));
        xorResult = BitUtils.xor(bits);
        Assert.assertTrue(xorResult.getValue() == false);
    }

    @Test
    public void testMDE() {
        Set<Bit> bits = new HashSet<Bit>(BitUtils.bitstring2BitList("0101"));
        Bit mdeResult = BitUtils.mde(bits);
        Assert.assertTrue(mdeResult.getValue() == false);

        bits = new HashSet<Bit>(BitUtils.bitstring2BitList("0111"));
        mdeResult = BitUtils.mde(bits);
        Assert.assertTrue(mdeResult.getValue() == true);
    }

    @Test
    public void testXorShift() {
        String bitString = "010101";
        List<Bit> bits = BitUtils.bitstring2BitList(bitString);
        Bit bit = BitUtils.shiftWithXOR(bits, new Bit(true), new Bit(true), 2, 5);
        System.out.println("Removed: " + bit.getValueInt());
        String bitStringXOR = BitUtils.toBitString(bits);
        System.out.println("With XOR: " + bitString + " -> " + bitStringXOR);
        //TODO : assert

        bits = BitUtils.bitstring2BitList(bitString);
        BitUtils.shift(bits, new Bit(true));
        String bitStringNoXOR = BitUtils.toBitString(bits);
        System.out.println("Without XOR: " + bitString + " -> " + bitStringNoXOR);
        Assert.assertTrue(bitStringNoXOR.equals("101010"));

        Assert.assertTrue(!bitStringNoXOR.equals(bitStringXOR));
    }
}
