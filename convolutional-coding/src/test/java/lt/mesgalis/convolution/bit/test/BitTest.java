package lt.mesgalis.convolution.bit.test;

import lt.mesgalis.convolution.bit.Bit;
import lt.mesgalis.convolution.bit.BitUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Karolis on 2014-10-04.
 */
public class BitTest {

    @Test
    public void test_stringConversion() {
        String test = "testy test";
        String testBits = BitUtils.stringToBitString(test);
        System.out.println(test + " : " + testBits);
        System.out.println(BitUtils.bitstringToCharactersString(testBits));
        List<Bit> bits = BitUtils.bitstring2BitList(testBits);
        String strinFromBits = BitUtils.toBitString(bits);
        System.out.println(testBits + "\n" + strinFromBits);
        Assert.assertTrue(testBits.equals(strinFromBits));
        String back = BitUtils.bitstringToCharactersString(strinFromBits);
        System.out.println(test + " : " + back);
        Assert.assertTrue(back.equals(test));
    }

    @Test
    public void test_byteArrayToBitArray() {
        byte[] b = new byte[] {127, -127};
        List<Bit> bits = BitUtils.byteArray2BitArray(b);
        System.out.println("{" + b[0] + ", " + b[1] + "}" + " : " + BitUtils.toBitString(bits));
        Assert.assertTrue(bits.size() == 16);
    }

}
