package lt.mesgalis.convolution.coder.test;

import lt.mesgalis.convolution.bit.Bit;
import lt.mesgalis.convolution.bit.BitChannel;
import lt.mesgalis.convolution.bit.BitUtils;
import lt.mesgalis.convolution.coder.ConvolutionalDecoder;
import lt.mesgalis.convolution.coder.ConvolutionalEncoder;
import lt.mesgalis.convolution.coder.IDecoder;
import lt.mesgalis.convolution.coder.IEncoder;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Karolis on 2014-10-06.
 */
public class CoderTest {

    private static final double PROBABILLITY_OF_FAILURE = 0.06;

    @Test
    public void test_decode() {
        System.out.println("test_decode");
        String bitstring = "010101111";
        List<Bit> bits = BitUtils.bitstring2BitList(bitstring);
        IEncoder coder = new ConvolutionalEncoder();
        IDecoder decoder = new ConvolutionalDecoder();

        List<Bit> encodedBits = coder.encode(bits);
        List<Bit> decodedBits = decoder.decode(encodedBits);

        String decodedBitString = BitUtils.toBitString(decodedBits);
        System.out.println(bitstring + " -> " + BitUtils.toBitString(encodedBits));
        System.out.println(bitstring + " -> " + decodedBitString);

        Assert.assertTrue(bitstring.equals(decodedBitString));
    }

    @Test
    public void test_encodeText() {
        System.out.println("test_decodeText");

        String testString = "Testukas";
        String bitString = BitUtils.stringToBitString(testString);
        System.out.println(testString + " : " + bitString);

        List<Bit> bits = BitUtils.bitstring2BitList(bitString);

        IEncoder coder = new ConvolutionalEncoder();
        IDecoder decoder = new ConvolutionalDecoder();

        List<Bit> encoded = coder.encode(bits);
        String encodedString = BitUtils.bitstringToCharactersString(BitUtils.toBitString(encoded));
        System.out.println(testString + " : " + encodedString);

        List<Bit> decoded = decoder.decode(encoded);
        String decodedBitString = BitUtils.toBitString(decoded);
        System.out.println(testString + " : " + decodedBitString);
        String decodedString = BitUtils.bitstringToCharactersString(decodedBitString);
        System.out.println(testString + " : " + decodedString);

    }


    @Test
    public void test_encodeTextChannelled() {
        System.out.println("test_encodeTextChannelled");

        String testString = "Testukas";
        String bitString = BitUtils.stringToBitString(testString);
        System.out.println(testString + " : " + bitString);

        List<Bit> bits = BitUtils.bitstring2BitList(bitString);

        IEncoder coder = new ConvolutionalEncoder();
        IDecoder decoder = new ConvolutionalDecoder();

        List<Bit> encoded = coder.encode(bits);
        String encodedBitString = BitUtils.toBitString(encoded);
        System.out.println(testString + " : " + encodedBitString);
        String encodedString = BitUtils.bitstringToCharactersString(encodedBitString);
        System.out.println(testString + " : " + encodedString);

        BitChannel channel = new BitChannel(encoded);
        List<Bit> channeled = channel.retrieve(PROBABILLITY_OF_FAILURE);
        String channeledBitString = BitUtils.toBitString(channeled);
        System.out.println(testString + " : " + channeledBitString);
        String channeledString = BitUtils.bitstringToCharactersString(channeledBitString);
        System.out.println(testString + " : " + channeledString);

        System.out.println("encoded == channeled -> " + encodedBitString.equals(channeledBitString));

        List<Bit> decoded = decoder.decode(channeled);
        String decodedBitString = BitUtils.toBitString(decoded);
        System.out.println(testString + " : " + decodedBitString);
        String decodedString = BitUtils.bitstringToCharactersString(decodedBitString);
        System.out.println(testString + " : " + decodedString);
    }
}
