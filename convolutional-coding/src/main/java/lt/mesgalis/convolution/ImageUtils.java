package lt.mesgalis.convolution;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Klasė darbui su paveikslėliais.
 */
public class ImageUtils {

    public static final int HEADER_SIZE_BMP = 100;

    public static byte[] bufferedPicToByteArray(BufferedImage bufferedPic) throws IOException {
        byte[] imageInByte;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedPic, "bmp", baos);
        baos.flush();
        imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }

}
