package lt.mesgalis.convolution;

import lt.mesgalis.commons.swing.Outputter;
import lt.mesgalis.convolution.ui.MainWindow;

public class Main {

    public static void main(String[] args) {
        Outputter.getInstance().output("Convolutional coder by Karolis Jocevičius" +
                "\n-----------------------------------------");

        MainWindow dialog = new MainWindow();
        dialog.pack();
        dialog.setVisible(true);
//        System.exit(0);
    }
}
