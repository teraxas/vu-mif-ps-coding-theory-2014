package lt.mesgalis.convolution.bit;

/**
 * Objektas reprezentuojantis bit'ą
 */
public class Bit implements Cloneable {
    private boolean val;

    public Bit(char value) {
        setValue(value == '1');
    }

    public Bit(int value) {
        setValue(value == 1);
    }

    public Bit(boolean value) {
        setValue(value);
    }

    /**
     * Pakeičia bito reikšmę į priešingą
     */
    public void flip() {
        setValue(!val);
    }

    public void setValue(boolean value) {
        this.val = value;
    }

    public void setValue(int value) {
        this.val = value == 1;
    }

    public void setValue(Bit value) {
        this.val = value.getValue();
    }

    public Boolean getValue() {
        return val;
    }

    public int getValueInt() {
        return val ? 1 : 0;
    }

    @Override
    public String toString() {
        return val ? "1" : "0";
    }

    public char toChar() {
        return val ? '1' : '0';
    }

    @Override
    public Bit clone() {
        return new Bit(val);
    }

}
