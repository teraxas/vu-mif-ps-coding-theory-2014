package lt.mesgalis.convolution.bit;

import java.util.*;

/**
 * Klasė aprašanti veiksmus su Bit objektais
 */
public class BitUtils {

    /**
     * Paverčia baitų masyvą į Bit sąrašą
     * @param bytes
     * @return
     */
    public static List<Bit> byteArray2BitArray(byte[] bytes) {
        List<Bit> bits = new ArrayList<Bit>();
        for (int i = 0; i < bytes.length * 8; i++) {
            bits.add(new Bit((bytes[i / 8] & (1 << (7 - (i % 8)))) > 0));
        }
        return bits;
    }

    /**
     * Bitų simbolių eilutę paverčia į Bit sąrašą.
     * Jei simbolis nėra '1' arba '0', simbolis laikomas '0'.
     * @param bitstring Bitų simbolių eilutė
     * @return Bit 'ų sąrašas
     */
    public static List<Bit> bitstring2BitList(String bitstring) {
        List<Bit> bits = new ArrayList<Bit>();
        for (int i = 0; i < bitstring.length(); i++) {
            bits.add(new Bit(bitstring.charAt(i)));
        }
        return bits;
    }

    /**
     * Grąžina bitų simbolių eilutę.
     * @param bits Bitų sąrašas.
     * @return bitų simbolių eilutė
     */
    public static String toBitString(List<Bit> bits ) {
        return toBitString(bits, false);
    }

    /**
     * Grąžina bitų simbolių eilutę.
     * @param bits Bitų sąrašas.
     * @return palieka tarpus tarp baitų
     */
    public static String toBitString(List<Bit> bits, boolean readable) {
        StringBuilder builder = new StringBuilder();
        byte bitCount = 0;
        for (Bit b : bits) {
            if (readable) {
                if (bitCount == 8) {
                    builder.append(" ");
                    bitCount = 0;
                }
                bitCount++;
            }
            builder.append(b.toString());
        }
        return builder.toString();
    }

    /**
     * Bit'ų sąrašą paverčia į baitų masyvą
     * @param bits
     * @return
     */
    public static byte[] bitArrayToByteArray(List<Bit> bits) {
        if (bits.size() % 8 != 0) {
            throw new IllegalArgumentException("bits.size() mod 8 != 0");
        }

        byte[] bytes = new byte[bits.size()/8];
        String bitString = toBitString(bits);
        int bytesWritePointer = 0;

        for (int i = 0; i < bitString.length(); i+=8) {
            String byteString = bitString.substring(i, i + 8);
            Long decimal = Long.parseLong(byteString, 2);
            bytes[bytesWritePointer] = decimal.byteValue();
            bytesWritePointer++;
        }
        return bytes;
    }

    /**
     * Simbolių eilutę paverčia į bitų simbolių eilutę
     * @param input
     * @return
     */
    public static String stringToBitString(String input) {

        String bitString = "";

        for (int i = 0; i < input.length(); i++) {
            String singleCharBitString = Integer.toBinaryString((int)input.charAt(i));
            bitString += completeByteString(singleCharBitString);
        }

        return bitString;
    }

    /**
     * Bitų simbolių eilutę paverčia į simbolių eilutę
     * @param input
     * @return
     */
    public static String bitstringToCharactersString(String input) {

        String charactersString = "";

        for (int i = 0; i < input.length() / 8; i++) {
            int characterInt = Integer.parseInt(input.substring(i * 8, (i * 8) + 8), 2);
            charactersString = charactersString + (char)characterInt;
        }

        return charactersString;
    }

    /**
     * Užpildo baito string'ą nuliais
     * @param bitString
     * @return
     */
    public static String completeByteString(String bitString) {
        while (bitString.length() < 8) {
            bitString = "0" + bitString;
        }
        return bitString;
    }

    /**
     * Pastumia bitus ir prideda naują pradžioje
     * @param bits  bitai
     * @param bit naujas bitas
     * @return
     */
    public static Bit shift(List<Bit> bits, Bit bit) {
        int tmpValue = 0;
        int setValue = bit.getValueInt();
        for (Bit r : bits) {
            tmpValue = r.getValueInt();
            r.setValue(setValue);
            setValue = tmpValue;
        }
        return new Bit(tmpValue);
    }

    /**
     * Pastumia bitus ir prideda naują pradžioje.
     * Įvykdo XOR operaciją nurodytiems bitams.
     * @param bits bitai
     * @param newBit naujas Bit
     * @param xorBit Bitas su kuriuo vykdomas XOR
     * @param xorIndexes bitų, su kuriais vykdomas XOR, indeksai
     * @return Išstumtas bit
     */
    public static Bit shiftWithXOR(List<Bit> bits, Bit newBit, Bit xorBit, int...xorIndexes) {
        Arrays.sort(xorIndexes);

        Bit tmpValue = new Bit(0);
        Bit setValue = newBit;

        for (Bit r : bits) {
            tmpValue.setValue(r);

            if (Arrays.binarySearch(xorIndexes, bits.indexOf(r)) != -1) {
                Set<Bit> xorBits = new HashSet<Bit>();
                xorBits.add(xorBit);
                xorBits.add(setValue);
                setValue = BitUtils.xor(xorBits);
            }

            r.setValue(setValue);
            setValue = tmpValue;
        }
        return tmpValue;
    }

    /**
     * XOR operacija.
     * Rezultatas lygus 1, jei X=1, Y=0 arba X=0, Y=1.
     * @param bits
     * @return
     */
    public static Bit xor(Set<Bit> bits) {
        int count = 0;
        for (Bit b : bits) {
            if (b.getValue()) {
                count++;
            }
        }

        return new Bit(count % 2 != 0);
    }

    /**
     * MDE operacija.
     * Rezutatas = 1, jei dauguma yra Bit'ų = 1
     * @param bits
     * @return
     */
    public static Bit mde(Set<Bit> bits) {
        int count1 = 0;
        for (Bit b : bits) {
            if (b.getValue()) {
                count1++;
            }
        }

        return new Bit(count1 > (bits.size() - count1));
    }

}
