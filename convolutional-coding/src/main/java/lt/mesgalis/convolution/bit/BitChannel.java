package lt.mesgalis.convolution.bit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Klasė reprezentuojanti perdavimo kanalą.
 */
public class BitChannel {

    private List<Bit> bits;

    private int errorCount;

    private List<Integer> errorPositions;

    public BitChannel() {}
    public BitChannel(List<Bit> bits) {
        this.setBits(bits);
    }

    /**
     * Srauto priėmimo metodas, nustatantis klaidų pozicijas.
     * Skirtas mažiems duomenų kiekiams.
     * Nustato klaidų kiekį - errorCount.
     * @param probabilityOfFailure klaidos tikimybė
     * @return gauti duomenys
     */
    public List<Bit> retrievePrecise(double probabilityOfFailure) {
        if (bits == null) {
            return null;
        }
        errorCount = 0;
        errorPositions = new ArrayList<Integer>();

        Random rnd = new Random();
        List<Bit> output = new ArrayList<Bit>();

        Iterator<Bit> iterator = bits.iterator();

        while (iterator.hasNext()) {
            Bit b = iterator.next();
            if (rnd.nextDouble() < probabilityOfFailure) {
                Bit clone = b.clone();
                clone.flip();
                output.add(clone);
                errorCount++;
                errorPositions.add(bits.indexOf(b));
            } else {
                output.add(b);
            }
        }

        return output;
    }

    /**
     * Srauto priėmimo metodas.
     * Nustato klaidų kiekį - errorCount.
     * @param probabilityOfFailure klaidos tikimybė
     * @return gauti duomenys
     */
    public List<Bit> retrieve(double probabilityOfFailure) {
        return retrieve(probabilityOfFailure, 0);
    }

    /**
     * Srauto priėmimo metodas.
     * Nustato klaidų kiekį - errorCount.
     * @param probabilityOfFailure klaidos tikimybė
     * @param safeBitLimit bitas iki kurio klaida negalima - tarnybiniai duomenys.
     * @return gauti duomenys
     */
    public List<Bit> retrieve(double probabilityOfFailure, int safeBitLimit) {
        if (bits == null) {
            return null;
        }
        errorPositions = null;
        errorCount = 0;

        Random rnd = new Random();
        List<Bit> output = new ArrayList<Bit>();

        Iterator<Bit> iterator = bits.iterator();

        int index = 0;
        while (index < safeBitLimit && iterator.hasNext()) {
            Bit b = iterator.next();
            output.add(b);
            index++;
        }

        while (iterator.hasNext()) {
            Bit b = iterator.next();
            if (rnd.nextDouble() < probabilityOfFailure) {
                Bit clone = b.clone();
                clone.flip();
                output.add(clone);
                errorCount++;
            } else {
                output.add(b);
            }
        }

        return output;
    }

    /**
     * Gražina nepakeistus duomenis
     * @return gauti duomenys
     */
    public List<Bit> retrieve() {
        errorCount = 0;
        return getBits();
    }

    /**
     * @return klaidų pozicijos String pavidalu
     */
    public String getErrorPositionsString() {
        StringBuilder buildder = new StringBuilder();
        for (Integer pos : errorPositions) {
            buildder.append(pos + " ");
        }
        return buildder.toString();
    }

    /**
     * @return klaidų pozicijų sąrašas
     */
    public List<Integer> getErrorPositions() {
        return errorPositions;
    }

    /**
     * @return paskutinio perdavimo klaidų skaičius
     */
    public int getLastErrorCount() {
        return errorCount;
    }

    public List<Bit> getBits() {
        return bits;
    }

    public void setBits(List<Bit> bits) {
        this.bits = bits;
    }
}
