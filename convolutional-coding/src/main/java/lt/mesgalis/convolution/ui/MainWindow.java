package lt.mesgalis.convolution.ui;

import lt.mesgalis.commons.swing.Outputter;
import lt.mesgalis.commons.swing.SwingUtils;
import lt.mesgalis.convolution.ImageUtils;
import lt.mesgalis.convolution.bit.Bit;
import lt.mesgalis.convolution.bit.BitChannel;
import lt.mesgalis.convolution.bit.BitUtils;
import lt.mesgalis.convolution.coder.ConvolutionalDecoder;
import lt.mesgalis.convolution.coder.ConvolutionalEncoder;
import lt.mesgalis.convolution.coder.IDecoder;
import lt.mesgalis.convolution.coder.IEncoder;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.NumberFormat;
import java.util.List;

/**
 * Pagrindinė GUI klasė.
 * Sudaryta iš ActionListener'ių, kviečiančių metodus Encoder/Decoder/BitUtils ir kitose klasėse.
 */
public class MainWindow extends JFrame {
    final JFileChooser fc = new JFileChooser();
    private JPanel contentPane;
    private JTabbedPane tabbedPane1;
    private JTextField vectorField;
    private JButton encodeButton;
    private JTextArea encodedField1;
    private JButton sendButton;
    private JTextField probabilityField1;
    private JTextArea receivedField1;
    private JButton decodeButton;
    private JTextField decodedField1;
    private JTextField textField;
    private JButton encodeButton1;
    private JButton sendButton1;
    private JButton decodeButton1;
    private JTextField encodedField;
    private JTextField receivedField;
    private JTextField decodedField;
    private JTextField probabilityField;
    private JTextField binaryField1;
    private JTextField binaryField2;
    private JButton openButton;
    private JTextField filenameField;
    private JTextField probabilityField2;
    private JButton encodeButton2;
    private JButton sendSimpleButton;
    private JButton sendSimpleButton1;
    private JButton toBinButton;
    private JButton sendWithoutEncodingButton;
    private JTextField errorsField2;
    private JTextField errorsField1;
    private JTextField errorsField;
    private JTextField errorPositionsField;

    private IEncoder encoder = new ConvolutionalEncoder();
    private IDecoder decoder = new ConvolutionalDecoder();
    private BitChannel channel = new BitChannel();

    private BufferedImage bufferedPic;

    public MainWindow() {
        setContentPane(contentPane);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        NumberFormat amountFormat = NumberFormat.getNumberInstance();

        // -- VECTOR --
        encodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = vectorField.getText();
                List<Bit> bits = BitUtils.bitstring2BitList(val);
                List<Bit> encodedBits = encoder.encode(bits);
                encodedField.setText(BitUtils.toBitString(encodedBits));
            }
        });
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = encodedField.getText();
                List<Bit> bits = BitUtils.bitstring2BitList(val);
                channel.setBits(bits);
                try {
                    bits = channel.retrievePrecise(Double.valueOf(probabilityField.getText()));
                    errorsField.setText(String.valueOf(channel.getLastErrorCount()));
                    errorPositionsField.setText(channel.getErrorPositionsString());
                } catch (NumberFormatException ex) {
                    SwingUtils.displayMessage("Netinkamas formatas");
                    return;
                }
                receivedField.setText(BitUtils.toBitString(bits));
            }
        });
        sendSimpleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = vectorField.getText();
                List<Bit> bits = BitUtils.bitstring2BitList(val);
                channel.setBits(bits);
                try {
                    bits = channel.retrievePrecise(Double.valueOf(probabilityField.getText()));
                    errorsField.setText(String.valueOf(channel.getLastErrorCount()));
                    errorPositionsField.setText(channel.getErrorPositionsString());
                } catch (NumberFormatException ex) {
                    SwingUtils.displayMessage("Netinkamas formatas");
                    return;
                }
                receivedField.setText(BitUtils.toBitString(bits));
                decodedField.setText(BitUtils.toBitString(bits));
            }
        });
        decodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = receivedField.getText();
                List<Bit> bits = BitUtils.bitstring2BitList(val);
                bits = decoder.decode(bits);
                decodedField.setText(BitUtils.toBitString(bits));
            }
        });
        // -- TEXT --
        encodeButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String bitString = BitUtils.stringToBitString(textField.getText());
                binaryField1.setText(bitString);
                List<Bit> bits = BitUtils.bitstring2BitList(bitString);
                bits = encoder.encode(bits);
                encodedField1.setText(BitUtils.toBitString(bits));
            }
        });
        toBinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String bitString = BitUtils.stringToBitString(textField.getText());
                binaryField1.setText(bitString);
            }
        });
        sendButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = encodedField1.getText();
                List<Bit> bits = BitUtils.bitstring2BitList(val);
                channel.setBits(bits);
                try {
                    bits = channel.retrieve(Double.valueOf((String) probabilityField1.getText()));
                    errorsField1.setText(String.valueOf(channel.getLastErrorCount()));
                } catch (NumberFormatException ex) {
                    SwingUtils.displayMessage("Netinkamas formatas");
                    return;
                }
                receivedField1.setText(BitUtils.toBitString(bits));
            }
        });
        sendSimpleButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = binaryField1.getText();
                List<Bit> bits = BitUtils.bitstring2BitList(val);
                channel.setBits(bits);
                try {
                    bits = channel.retrieve(Double.valueOf((String) probabilityField1.getText()));
                    errorsField1.setText(String.valueOf(channel.getLastErrorCount()));
                } catch (NumberFormatException ex) {
                    SwingUtils.displayMessage("Netinkamas formatas");
                    return;
                }
                receivedField1.setText(BitUtils.toBitString(bits));
                decodedField1.setText(BitUtils.bitstringToCharactersString(receivedField1.getText()));
            }
        });
        decodeButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = receivedField1.getText();
                List<Bit> bits = BitUtils.bitstring2BitList(val);
                bits = decoder.decode(bits);
                String bitString = BitUtils.toBitString(bits);
                binaryField2.setText(bitString);
                decodedField1.setText(BitUtils.bitstringToCharactersString(bitString));
            }
        });
        // -- IMAGES --
        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == openButton) {
                    int returnVal = fc.showOpenDialog(MainWindow.this);

                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();
                        try {
                            bufferedPic = ImageIO.read(file);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                            return;
                        }

                        filenameField.setText(file.getPath());
                        new ImageView(bufferedPic, "Original");
                        Outputter.getInstance().output("Opening: " + file.getName() + ".");
                    } else {
                        Outputter.getInstance().output("Open command cancelled by user.");
                    }
                }
            }
        });
        encodeButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                imageSend(true);
            }
        });


        sendWithoutEncodingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                imageSend(false);
//                imageReDisplay();
            }
        });
    }

    private void imageSend(boolean encode) {
        byte[] imageInByte = new byte[0];

//        StopWatch timer = new StopWatch();
//        timer.start();
        Outputter.getInstance().output("Sending image started");

        try {
            imageInByte = ImageUtils.bufferedPicToByteArray(bufferedPic);
        } catch (IOException e1) {
            e1.printStackTrace();
            SwingUtils.displayMessage("Error!");
            Outputter.getInstance().output("Error converting file");
        }

        List<Bit> bits = BitUtils.byteArray2BitArray(imageInByte);

        if (encode) {
//            timer.split();
//            Outputter.getInstance().output(timer.getSplitTime() + ": Sending image: encode");
            bits = encoder.encode(bits);
//            timer.split();
//            Outputter.getInstance().output(timer.getSplitTime() + ": Sending image: Encoded to List of size: " + bits.size());
        }

        channel.setBits(bits);
        List<Bit> processedPic;
        try {
//            timer.split();
//            Outputter.getInstance().output(timer.getSplitTime() + ": Sending image: retrieve from channel started");
            processedPic = channel.retrieve(Double.valueOf(probabilityField2.getText()), ImageUtils.HEADER_SIZE_BMP * 8 + 1);
            errorsField2.setText(String.valueOf(channel.getLastErrorCount()));
        } catch (NumberFormatException ex) {
            SwingUtils.displayMessage("Netinkamas formatas");
            return;
        }

        if (encode) {
//            timer.split();
//            Outputter.getInstance().output(timer.getSplitTime() + ": Sending image: decode started");
            processedPic = decoder.decode(processedPic);
        }

        byte[] bytes = BitUtils.bitArrayToByteArray(processedPic);

        InputStream in = new ByteArrayInputStream(bytes);
        try {
//            timer.split();
//            Outputter.getInstance().output(timer.getSplitTime() + ": Sending image: reading image");
            BufferedImage bImageFromConvert = ImageIO.read(in);
            new ImageView(bImageFromConvert, encode ? "Encoded" : "Not encoded");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    // debug only
//    private void imageReDisplay() {
//        byte[] imageInByte = new byte[0];
//
//        StopWatch timer = new StopWatch();
//        timer.start();
//        Outputter.getInstance().output("Sending image started");
//
//        try {
//            imageInByte = ImageUtils.bufferedPicToByteArray(bufferedPic);
//        } catch (IOException e1) {
//            e1.printStackTrace();
//            SwingUtils.displayMessage("Error!");
//            Outputter.getInstance().output("Error converting file");
//        }
//
//        List<Bit> bits = BitUtils.byteArray2BitArray(imageInByte);
//        channel.setBits(bits);
//        bits = channel.retrieve(ImageUtils.HEADER_SIZE_BMP * 8 + 1);
//        byte[] bytes = BitUtils.bitArrayToByteArray(bits);
//
//        InputStream in = new ByteArrayInputStream(bytes);
//        try {
//            timer.split();
//            Outputter.getInstance().output(timer.getSplitTime() + ": Sending image: reading image");
//            BufferedImage bImageFromConvert = ImageIO.read(in);
//            new ImageView(bImageFromConvert, "Redisplayed");
//        } catch (Exception e1) {
//            e1.printStackTrace();
//            Outputter.getInstance().output(timer.getSplitTime() + ": Sending image: error: " + e1.getMessage());
//        }
//    }


}
