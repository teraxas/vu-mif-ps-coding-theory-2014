package lt.mesgalis.convolution.ui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

/**
 * Paveikslėlio rodymo langas.
 */
public class ImageView extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel imageLabel;

    public ImageView(BufferedImage image, String name) {
        setContentPane(contentPane);
        setModal(false);
        getRootPane().setDefaultButton(buttonOK);

        imageLabel.setIcon(new ImageIcon(image));
        setVisible(true);
        setSize(image.getWidth() + 100, image.getHeight() + 100);
        setName(name);
        setTitle(name);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
    }

    private void onOK() {
        dispose();
    }

}
