package lt.mesgalis.convolution.coder;

import lt.mesgalis.convolution.bit.Bit;
import lt.mesgalis.convolution.bit.BitUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Convolutional encoder
 */
public class ConvolutionalEncoder implements IEncoder {

    public static final Logger log = Logger.getLogger(ConvolutionalEncoder.class.getName());

    public static final int DEFAULT_REGISTERS_COUNT = 6;

    private int registersCount;
    private List<Bit> registers;

    public ConvolutionalEncoder() {
        this(DEFAULT_REGISTERS_COUNT);
    }

    private ConvolutionalEncoder(int registers) {
        this.registers = new ArrayList<Bit>();
        registersCount = registers;
        for (int i = 0; i < registers; i++) {
            this.registers.add(new Bit(0));
        }
    }

    /**
     * Duomenų užkodavimas
     * @param input
     * @return
     */
    @Override
    public List<Bit> encode(List<Bit> input) {
        List<Bit> copy = new ArrayList<Bit>(input);
        copy.addAll(BitUtils.bitstring2BitList("000000"));
        List<Bit> result = encodeBitList(copy);
        if (result.size() < 200) {
            log.fine("Encoded: " + BitUtils.toBitString(result));
        }
        return result;
    }

    private  List<Bit> encodeBitList(List<Bit> input) {
        List<Bit> result = new ArrayList<Bit>();
        for (Bit b : input) {
            Set<Bit> xorSet = new HashSet<Bit>();
            xorSet.add(registers.get(1));
            xorSet.add(registers.get(4));
            xorSet.add(registers.get(5));
            xorSet.add(b);

            Bit xorResult = BitUtils.xor(xorSet);
            result.add(b);
            result.add(xorResult);

            BitUtils.shift(registers, b);
        }

        cleanRegisters();
        return result;
    }

    private void cleanRegisters() {
        for (Bit r : registers) {
            r.setValue(0);
        }
    }

    public int getRegistersCount() {
        return registersCount;
    }
}
