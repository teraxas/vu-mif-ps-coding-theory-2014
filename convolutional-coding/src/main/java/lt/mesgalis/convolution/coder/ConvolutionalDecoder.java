package lt.mesgalis.convolution.coder;

import lt.mesgalis.convolution.bit.Bit;
import lt.mesgalis.convolution.bit.BitUtils;

import java.util.*;

/**
 * Convolutional decoder
 */
public class ConvolutionalDecoder implements IDecoder {

    public static final int DEFAULT_REGISTERS_COUNT = 6;

    private int registersCount;
    private List<Bit> registers;
    private List<Bit> errorRegisters;

    private Bit bit1 = null;
    private Bit bit2 = null;

    public ConvolutionalDecoder() {
        this(DEFAULT_REGISTERS_COUNT);
    }

    private ConvolutionalDecoder(int registers) {
        this.registers = new ArrayList<Bit>();
        this.errorRegisters = new ArrayList<Bit>();
        registersCount = registers;
        for (int i = 0; i < registers; i++) {
            this.registers.add(new Bit(0));
            this.errorRegisters.add(new Bit(0));
        }
    }

    /**
     * Duomenų dekodavimas
     * @param input
     * @return
     */
    @Override
    public List<Bit> decode(List<Bit> input) {
        List<Bit> result = new ArrayList<Bit>();
        Iterator<Bit> bits = input.iterator();

        while (bits.hasNext()) { //Dekodavimui imama po du bitus žingsniui
            bit1 = bits.next();
            bit2 = bits.next();

            result.add(decode(bit1, bit2));
        }

        int removeCounter = 6;
        while (removeCounter > 0 && result.size() >= removeCounter) {
            removeCounter--;
            result.remove(removeCounter);
        }

        cleanRegisters(); //Baigus valom registrus
        return result;
    }

    /**
     * Dekodavimas dviems bitams.
     * @param bit1
     * @param bit2
     * @return dekoduotas bitas
     */
    private Bit decode(Bit bit1, Bit bit2) {
        Set<Bit> toXOR = new HashSet<Bit>();
        toXOR.add(bit1);
        toXOR.add(bit2);
        toXOR.add(registers.get(1));
        toXOR.add(registers.get(4));
        toXOR.add(registers.get(5));
        Bit xorResult = BitUtils.xor(toXOR);

        Set<Bit> toMDE = new HashSet<Bit>();
        toMDE.add(xorResult);
        toMDE.add(errorRegisters.get(0));
        toMDE.add(errorRegisters.get(3));
        toMDE.add(errorRegisters.get(5));
        Bit mdeResult = BitUtils.mde(toMDE);

        Bit lastVal = BitUtils.shift(registers, bit1);
//        BitUtils.shiftWithXOR(errorRegisters, xorResult, mdeResult, 0, 1, 4); //feedback
        BitUtils.shift(errorRegisters, xorResult);

        Set<Bit> toXOR2 = new HashSet<Bit>();
        toXOR2.add(mdeResult);
        toXOR2.add(lastVal);
        return BitUtils.xor(toXOR2);
    }

    private void cleanRegisters() {
        for (Bit r : registers) {
            r.setValue(0);
        }
        for (Bit r : errorRegisters) {
            r.setValue(0);
        }
    }

    public int getRegistersCount() {
        return registersCount;
    }

}
