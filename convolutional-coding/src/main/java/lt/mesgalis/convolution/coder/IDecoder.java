package lt.mesgalis.convolution.coder;

import lt.mesgalis.convolution.bit.Bit;

import java.util.List;

/**
 * Dekoderio interfeisas
 */
public interface IDecoder {

    /**
     * Dekoduoja Bit sąrašą
     * @param input
     * @return
     */
    List<Bit> decode(List<Bit> input);
}
