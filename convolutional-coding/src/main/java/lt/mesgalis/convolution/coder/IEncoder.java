package lt.mesgalis.convolution.coder;

import lt.mesgalis.convolution.bit.Bit;

import java.util.List;

/**
 * Enkoderio interfeisas
 */
public interface IEncoder {

    /**
     * Užkoduoja Bit sąrašą
     * @param input
     * @return
     */
    List<Bit> encode(List<Bit> input);
}
